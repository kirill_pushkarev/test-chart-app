import * as React from 'react';
import {
  Button,
  Col,
  Container,
  Jumbotron,
  Row
} from 'reactstrap';

class Home extends React.Component {
    public render() {
      return  (
        <Jumbotron>
          <Container>
            <Row>
              <Col>
                <h1>Welcome Home!</h1>
                <p>
                  <Button
                    tag="a"
                    color="success"
                    size="large"
                    href="/report"
                  >
                    View Report
                  </Button>
                </p>
              </Col>
            </Row>
          </Container>
        </Jumbotron>
      );
    }
  }

export default Home;