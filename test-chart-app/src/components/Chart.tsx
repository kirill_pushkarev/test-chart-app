import * as React from 'react';
import * as ReactHighcharts from 'react-highcharts';
import products, { DataRow } from '../fixture';
import { ViewMode } from './Report';

export interface ChartProps { data: DataRow[], selectedYear: number, selectedViewMode: ViewMode }

class Chart extends React.Component<ChartProps, {}> {
  constructor(props : ChartProps) {
    super(props);
  }

  public render() {
    const productsFilteredByYear =  products.filter(dataRow => dataRow.year === this.props.selectedYear);
    let series: any = [];
    let tooltip;

    if (this.props.selectedViewMode === ViewMode.Goods){
      series = productsFilteredByYear.map(dataRow => {
        return {
        name: dataRow.name, 
        data: [[dataRow.feature1, dataRow.feature2]]};
      })

      tooltip = {
        headerFormat: '<b>{series.name}</b><br>',
        pointFormat: 'Feature 1 : {point.x}, Feature 2 : {point.y}'
      }
    }
    else if (this.props.selectedViewMode === ViewMode.GoodsCategories) {
      series = [
        {
          name: "feature1 < 100",
          data: []
        },
        {
          name: "100 <= feature1 <= 150",
          data: []
        },
        {
          name: "feature1 > 150",
          data: []
        }
      ];
      productsFilteredByYear.forEach(dataRow => {
        if (dataRow.feature1 < 100) {
          series[0].data.push({name: dataRow.name, x: dataRow.feature1, y: dataRow.feature2})
        }
        else if (dataRow.feature1 >= 100 && dataRow.feature1 <= 150) {
          series[1].data.push({name: dataRow.name, x: dataRow.feature1, y: dataRow.feature2})
        }
        else if (dataRow.feature1 > 150) {
          series[2].data.push({name: dataRow.name, x: dataRow.feature1, y: dataRow.feature2})
        }
      });

      tooltip = {
        headerFormat: '<i>{series.name}</i><br>',
        pointFormat: '<b>{point.name}</b><br> Feature 1 : {point.x}, Feature 2 : {point.y}'
      }
    }

    const config = {
      chart: {
        type: 'scatter',
        zoomType: 'xy'
      },
      title: {
        text: 'Goods'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        title: {
          text: 'Feature 1'
        },
        startOnTick: true,
        endOnTick: true,
        showLastLabel: true
      },
      yAxis: {
        title: {
          text: 'Feature 2'
        }
      },
      legend: {
        layout: 'vertical',
        align: 'left',
        verticalAlign: 'top',
        x: 100,
        y: 70,
        floating: true,
        backgroundColor: '#FFFFFF',
        borderWidth: 1
      },
      plotOptions: {
        scatter: {
          marker: {
            radius: 5,
            states: {
              hover: {
                enabled: true,
                lineColor: 'rgb(100,100,100)'
              }
            }
          },
          states: {
            hover: {
              marker: {
                enabled: false
              }
            }
          },
          tooltip
        },
      },
      series
    };

    return  (
      <ReactHighcharts config={config}/>
    );
  }
}

  export default Chart;