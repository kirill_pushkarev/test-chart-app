import * as React from 'react';
import products, { DataRow } from '../fixture';
import Chart from './Chart';
import ViewModeSelect from './ViewModeSelect';
import YearSelect from './YearSelect';

export enum ViewMode { Goods = 1, GoodsCategories = 2 };
export interface ReportProps { data: DataRow[] }
interface State  { selectedYear: number; selectedViewMode: ViewMode}
const getAllYears = () => [2016, 2015];

class Report extends React.Component<ReportProps, State> {
  public state: State = {
    selectedYear: Math.max(...getAllYears()),
    selectedViewMode: ViewMode.Goods
  };

  public handleYearChange = (selectedYear: number) => {
    this.setState({
      selectedYear
    });
  }

  public handleViewModeChange = (selectedViewMode: ViewMode) => {
    this.setState({
      selectedViewMode
    });
  }

  public render() {
    return  (
      <div>
        <Chart data={products} selectedYear={this.state.selectedYear} selectedViewMode={this.state.selectedViewMode}/>
        <YearSelect years={getAllYears()} selectedYear={this.state.selectedYear} handleYearChange={this.handleYearChange}/>
        <ViewModeSelect selectedViewMode={this.state.selectedViewMode} handleViewModeChange={this.handleViewModeChange}/>
      </div>
    );
  }
}

  export default Report;