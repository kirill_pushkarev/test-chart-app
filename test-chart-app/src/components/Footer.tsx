import * as React from 'react';

class Footer extends React.Component {
    public render() {
      return  (
        <footer className="App-footer">
            <div>Smart Analytics 2018</div>
        </footer>
      );
    }
  }

export default Footer;