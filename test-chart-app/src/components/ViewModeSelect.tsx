import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import { Input } from 'reactstrap';
import { ViewMode } from './Report';


export interface ViewModeSelectProps { selectedViewMode: ViewMode, handleViewModeChange: any};

class ViewModeSelect extends React.Component<ViewModeSelectProps, {}> {
  constructor(props : ViewModeSelectProps) {
    super(props);
    this.handleViewModeChange = this.handleViewModeChange.bind(this);
  }

  public handleViewModeChange(e: any) {
    const viewMode : ViewMode = parseInt(e.target.value, 10);
    this.props.handleViewModeChange(viewMode);
  }

  public render() {
    return  (
      <div>
        <Input value={this.props.selectedViewMode} onChange={this.handleViewModeChange} type="select" bsSize="sm" className="w-25">
          <option value={ViewMode.Goods}>Goods</option>
          <option value={ViewMode.GoodsCategories}>Goods Categories</option>
        </Input>
      </div>
    );
  }
}

export default ViewModeSelect;