import 'bootstrap/dist/css/bootstrap.min.css';
import * as React from 'react';
import { Route, Switch } from "react-router-dom";
// import '../App.css';
import Footer from './Footer';
import Header from './Header';
import Home from "./Home"
import Report from "./Report"

class App extends React.Component {
  public render() {
    return (
      <div>
        <Header />

        <Switch>
          <Route exact={true} path="/" component={Home} />
          <Route path="/report" component={Report}/>
        </Switch>

        <Footer />
      </div>
    );
  }
}

export default App;
